#include <SDL2/SDL.h>
#include <stdio.h>

/************************************/
/*  exemple de création de fenêtres */
/************************************/

int main(int argc, char **argv)
{
    (void)argc;
    (void)argv;

  

    /* Initialisation de la SDL  + gestion de l'échec possible */
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
    {
        SDL_Log("Error : SDL initialisation - %s\n",
                SDL_GetError()); // l'initialisation de la SDL a échoué
        exit(EXIT_FAILURE);
    }

    SDL_DisplayMode current;     //taille de ma fenêtre.
    if (SDL_GetCurrentDisplayMode(0,&current)!=0)
    {
        exit(EXIT_FAILURE);
    }
    printf("%d %d\n",current.h,current.w);

  



    // SDL_Window* tabwindow[20];  //diagonale desceendante
    // for(int i=0;i<20;++i)
    // {
    //     tabwindow[i] = SDL_CreateWindow(
    //         "fenetre",
    //         96*i, 54*i,
    //         192, 108,
    //         0);
        
    // }
    // SDL_Window* tabwindow1[20];  //diagolane ascendante
    // for(int i=0;i<20;++i)
    // {
    //     tabwindow1[i] = SDL_CreateWindow(
    //         "fenetre",
    //         96*i, 972-54*i,
    //         192, 108,
    //         0);   
    // }
    // SDL_Delay(2000);
    // for (int i=0;i<20;++i)
    // {
    //     SDL_DestroyWindow(tabwindow1[i]);
    // }
    // for (int i=0;i<20;++i)
    // {
    //     SDL_DestroyWindow(tabwindow[i]);
    // }



    SDL_Window* fenetre1;  //plus original fenetre1
    SDL_Window* fenetre2;   //plus original fenetre2

    fenetre1= SDL_CreateWindow(
        "fenetre1",
        100, 300,
        192, 108,
        0);
    fenetre2= SDL_CreateWindow(
        "fenetre2",
        1620, 700,
        192, 108,
        0);

   for (int i=0;i<140;++i)
   {
    SDL_SetWindowPosition(fenetre1, 100+10*i, 300);
    SDL_SetWindowPosition(fenetre2, 1620-10*i, 700);
    SDL_Delay(50);
   }
   SDL_DestroyWindow(fenetre1);
   SDL_DestroyWindow(fenetre2);


    /* Normalement, on devrait ici remplir les fenêtres... */
    SDL_Delay(2000); // Pause exprimée  en ms

    SDL_Quit(); // la SDL

    return 0;
}
