#include <SDL2/SDL.h>
#include <stdio.h>

void draw(SDL_Renderer* dessin)
{
    SDL_Rect rect1; //magenta
    SDL_SetRenderDrawColor(dessin, 255, 0, 255, 255);  //rouge - vert - bleu
    rect1.x = 0;
    rect1.y = 0;
    rect1.w = 300;
    rect1.h = 500;
    SDL_RenderFillRect(dessin, &rect1);
    
    SDL_Rect rect2;  //cyan
    SDL_SetRenderDrawColor(dessin, 0, 255, 255, 255);  //rouge - vert - bleu
    rect2.x = 300;
    rect2.y = 0;
    rect2.w = 300;
    rect2.h = 500;
    SDL_RenderFillRect(dessin, &rect2);

    SDL_Rect rect3;  //jaune
    SDL_SetRenderDrawColor(dessin, 255, 255, 0, 255);  //rouge - vert - bleu
    rect3.x = 600;
    rect3.y = 0;
    rect3.w = 300;
    rect3.h = 500;
    SDL_RenderFillRect(dessin, &rect3);

    SDL_RenderPresent(dessin);
    SDL_Delay(5000);
}

void animation(SDL_Renderer* dessin2)
{
    for (int i=0;i<100;++i)
    {
        SDL_SetRenderDrawColor(dessin2, 0, 0, 0, 255);
        SDL_RenderClear(dessin2);
        SDL_RenderPresent(dessin2);

        SDL_Rect rect0;
        SDL_SetRenderDrawColor(dessin2, 0, 255, 255-2.5*i, 255);  //rouge - vert - bleu
        rect0.x = i*10;
        rect0.y = 0;
        rect0.w = 300;
        rect0.h = 500;
        SDL_RenderFillRect(dessin2, &rect0);

        SDL_Rect rect1;  //bouge pas
        SDL_SetRenderDrawColor(dessin2, 255, 255, 255, 255);  //rouge - vert - bleu
        rect1.x = 1290;
        rect1.y = 0;
        rect1.w = 300;
        rect1.h = 500;
        SDL_RenderFillRect(dessin2, &rect1);

        SDL_Rect rect2;  //VERT
        SDL_SetRenderDrawColor(dessin2, 140+i, 255, 140-i, 255);  //rouge - vert - bleu
        rect2.x = 600+i*10;
        rect2.y = 0;
        rect2.w = 300;
        rect2.h = 500;
        SDL_RenderFillRect(dessin2, &rect2);

        SDL_Rect rect3;  //bande
        SDL_SetRenderDrawColor(dessin2, 255-2.5*i, 2.5*i, 255-2.5*i, 255);
        rect3.x = 0;
        rect3.y = 500;
        rect3.w = 1920;
        rect3.h = 50;
        SDL_RenderFillRect(dessin2, &rect3);

        SDL_RenderPresent(dessin2);
        SDL_Delay(80);
    }
    SDL_Delay(2000);
    for (int i=0;i<100;++i)
    {
        SDL_SetRenderDrawColor(dessin2, 0, 0, 0, 255);
        SDL_RenderClear(dessin2);
        SDL_RenderPresent(dessin2);

        SDL_Rect rect0;
        SDL_SetRenderDrawColor(dessin2, 0, 255-2.5*i, 2*i, 255);  //rouge - vert - bleu
        rect0.x = 1000-i*10;
        rect0.y = 0;
        rect0.w = 300;
        rect0.h = 500;
        SDL_RenderFillRect(dessin2, &rect0);
    
        SDL_Rect rect1;  //bouge pas
        SDL_SetRenderDrawColor(dessin2, 255, 255, 255, 255);  //rouge - vert - bleu
        rect1.x = 1300-10*i;
        rect1.y = 0;
        rect1.w = 300;
        rect1.h = 500;
        SDL_RenderFillRect(dessin2, &rect1);

        SDL_Rect rect2;  //VERT
        SDL_SetRenderDrawColor(dessin2, 255, 255-2.5*i, 0, 255);  //rouge - vert - bleu
        rect2.x = 1600-i*10;
        rect2.y = 0;
        rect2.w = 300;
        rect2.h = 500;
        SDL_RenderFillRect(dessin2, &rect2);

        SDL_Rect rect3;  //bande
        SDL_SetRenderDrawColor(dessin2, 2.5*i, 255-2.5*i, 2.5*i, 255);
        rect3.x = 0;
        rect3.y = 500;
        rect3.w = 1920;
        rect3.h = 50;
        SDL_RenderFillRect(dessin2, &rect3);

        SDL_RenderPresent(dessin2);
        SDL_Delay(80);        
    }
    SDL_Delay(1500);
}

int main()
{
    SDL_Window* fenetre;
    fenetre= SDL_CreateWindow(
        "fenetre",
        0, 0,
        1920, 800,
        0);
    SDL_Renderer* dessin;
    dessin = SDL_CreateRenderer(fenetre, -1, 2);
    //draw(dessin);
    animation(dessin);
}